import { SingletonPromise, ResourceRef } from 'nui-utils';

const CacheMap = new WeakMap();

export default (NeueUI) => new (class BrowserLoader {
	constructor(){
		NeueUI.Logger.attach(this, 'browser-loader'); // Attach logger

		CacheMap.set(this, []);

		this.dispatcher = NeueUI.dispatcher;

		this.log("Started loader", this.LOG_LOUD);

		this.init();
	}

	init() {
		const Loader = this;
		var fns = [ window['webpackJsonp'] ];
		Object.defineProperty(window, 'webpackJsonp', {
			'enumerable': false,
			'configurabe': false,
			'set': (fn) => fns.push(fn),
			'get': () => function(init, modules, targets) {
				const args = [
					init,
					modules,
					targets.map((v,i) => {
						const fn = modules[v];
						modules[v] = function(module, exports, req) {
							fn(module, exports, req);
							module.exports(NeueUI);
							return;
						}
						return v;
					})
				];

				var ret;
				for(let fn of fns) {
					try {
						ret = fn(...args);
					} catch(e) {
						this.log(e);
						throw e;
					}
				}

				return ret;
			}
		});

		NeueUI.Dispatcher.when('nui.resource.import').then( (ref) => this.import(ref) );
	}

	import(ref) {
		const resRef = new ResourceRef(ref);

		const Cache = CacheMap.get(this);
		if(Cache.indexOf(String(resRef)) > -1) return;
		Cache.push(String(resRef));

		this.log(`Importing ${resRef}`, this.LOG_LOUD);

		return new SingletonPromise( (res,rej) => {
			try {
				var refPath = `${NeueUI.Config.appContext}compiled.${resRef.serialize()}.js`;

				this.log([resRef.toString(), `Creating import script: '${refPath}'`], this.LOG_DEBUG);

				let scrpt = document.createElement('script');

				scrpt.setAttribute('src', refPath);

				scrpt.addEventListener('error', (e) => {
					NeueUI.Dispatcher.resolve(`nui.resource.404`, resRef);
					NeueUI.Dispatcher.resolve(`nui.resource.404.${resRef}`, resRef);
					NeueUI.Dispatcher.resolve(`nui.resource.404.${resRef.type}`, resRef);
					rej(e);
				});

				document.body.appendChild(scrpt);
			} catch(e) {
				this.log(e);
				rej(e);
			}
		}, (e) => this.log(['ERROR',e.stack]));
	}
});
