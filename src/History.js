import { createBrowserHistory } from 'history'

export default (NeueUI) => {
	const history = createBrowserHistory();

	/* Listen for changes to the current location.
	const unlisten = history.listen((location, action) => {
		NeueUI.Dispatcher.resolve('navigate', location.pathname);
	});*/

	NeueUI.Dispatcher.when('navigate').then( (path) => {
		if(path.match(/^\/[0-9]{3}$/) === null) history.push(path);
		return path;
	});

	window.NeueUI = NeueUI;
}
